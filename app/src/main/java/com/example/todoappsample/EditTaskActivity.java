package com.example.todoappsample;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Calendar;

/**
 * This activity is launched from MainActivity.java to provide a user interface
 * so that the user can change the existing data of a task. This changed data
 * is then sent back to MainActivity.java as it is the only activity with access
 * to the view model.
 * @author John Fielding
 */
public class EditTaskActivity extends AppCompatActivity {

    // Define string id's for components of the result intent
    public static final String EXTRA_EDIT_TASK_ID = "com.example.android.tasklistsql.EDIT_TASK_ID";
    public static final String EXTRA_EDIT_TASK_NAME = "com.example.android.tasklistsql.EDIT_TASK_NAME";
    public static final String EXTRA_EDIT_DUE_DATE = "com.example.android.tasklistsql.EDIT_DUE_DATE";
    public static final String EXTRA_EDIT_COMPLETION = "com.example.android.tasklistsql.EDIT_IS_COMPLETED";

    // Stores handles for interactable UI elements
    private EditText mEditTaskView;
    private EditText mEditDateView;
    private CheckBox mEnableDateView;
    private CheckBox mIsCompletedView;
    private DatePickerDialog mDatePickerView;
    private int id;

    /**
     * On activity's creation, selected tasks details are imported into UI
     * and input listeners are set up.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_task);
        mEditTaskView = findViewById(R.id.edit_task);
        mEditDateView = findViewById(R.id.edit_date);
        mEnableDateView = findViewById(R.id.edit_date_picker_checkbox);
        mIsCompletedView = findViewById(R.id.edit_completed_checkbox);
        Intent intent = getIntent();

        id = intent.getIntExtra(EXTRA_EDIT_TASK_ID, -1);
        String tmpName = intent.getStringExtra(EXTRA_EDIT_TASK_NAME);
        String tmpDate = intent.getStringExtra(EXTRA_EDIT_DUE_DATE);
        Boolean tmpCompleted = intent.getBooleanExtra(EXTRA_EDIT_COMPLETION, false);

        mEditTaskView.setText(tmpName);

        mEnableDateView.setChecked(!TextUtils.isEmpty(tmpDate));
        if(mEnableDateView.isChecked()){                // check if task has a date
            mEditDateView.setVisibility(View.VISIBLE);  // if so make date view UI visible
            mEditDateView.setText(tmpDate);
        } else {
            mEditDateView.setVisibility(View.GONE);     // if not remove date view UI element
        }

        mIsCompletedView.setChecked(tmpCompleted);

        // on click listener for user to toggle visibility of date view
        mEnableDateView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mEnableDateView.isChecked()){
                    mEditDateView.setVisibility(View.VISIBLE);
                } else {
                    mEditDateView.setVisibility(View.GONE);
                }
            }
        });

        mEditDateView.setInputType(InputType.TYPE_NULL);
        // on click listener to open date picker when date view is tapped
        mEditDateView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar calendar = Calendar.getInstance();
                int day = calendar.get(Calendar.DAY_OF_MONTH);
                int month = calendar.get(Calendar.MONTH);
                int year = calendar.get(Calendar.YEAR);
                // TODO: if there is existing date make that the default selection in picker

                mDatePickerView = new DatePickerDialog(EditTaskActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                mEditDateView.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                            }
                        }, year, month, day);
                mDatePickerView.show();
            }
        });


        final Button button = findViewById(R.id.edit_button_save);
        // on click listener to save changes to selected todo task
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent replyIntent = new Intent();
                if (TextUtils.isEmpty(mEditTaskView.getText())||(mEnableDateView.isChecked() && TextUtils.isEmpty(mEditDateView.getText()))||id == -1) {
                    Toast.makeText(getApplicationContext(),"Error: Either task name and/or due date is enabled but empty",Toast.LENGTH_LONG).show();    // TODO: change to popup dialogue box
                } else if (id == -1) {
                    Toast.makeText(getApplicationContext(),"Error: Selected todo Task has invalid id if error persists contact support",Toast.LENGTH_LONG).show();
                    setResult(RESULT_CANCELED, replyIntent);        //TODO: add cancel button with this as the result
                } else {
                    // there are no issues with this data and will changes are sent to the main activity to be saved
                    String task = mEditTaskView.getText().toString();
                    Boolean completionStatus = mIsCompletedView.isChecked();
                    replyIntent.putExtra(EXTRA_EDIT_TASK_ID, id);
                    replyIntent.putExtra(EXTRA_EDIT_TASK_NAME, task);
                    replyIntent.putExtra(EXTRA_EDIT_COMPLETION, completionStatus);
                    if(mEnableDateView.isChecked()){
                        String date = mEditDateView.getText().toString();
                        replyIntent.putExtra(EXTRA_EDIT_DUE_DATE, date);
                    } else {
                        replyIntent.putExtra(EXTRA_EDIT_DUE_DATE, "");
                    }
                    setResult(RESULT_OK, replyIntent);
                }
                finish();
            }
        });
    }
}
package com.example.todoappsample;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.List;
/**
 * This application follows an MVVM architectural pattern implementing
 * android jetpack. Implementing a view-model that seperates the data's 
 * presentation (i.e. the view) from the actual data (i.e. the model). 
 * 
 * This activity acts as the View and handles displaying data from the
 * view-model,transitioning to other activities to edit or create a todo
 * task and communicating results from these activities to the view model 
 * so the data can be updated.
 * @author John Fielding
 */
public class MainActivity extends AppCompatActivity {

    private TaskViewModel mTaskViewModel;
    public static final int NEW_TASK_ACTIVITY_REQUEST_CODE = 1;
    public static final int EDIT_TASK_ACTIVITY_REQUEST_CODE = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Sets up a new floating button to add a new task to the database
        FloatingActionButton newTaskButton = findViewById(R.id.fab);
        newTaskButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, NewTaskActivity.class);
                startActivityForResult(intent, NEW_TASK_ACTIVITY_REQUEST_CODE);       // Starts NewTaskActivity see NewTaskActivity.java for details
            }
        });

        // Sets up recycler view with collapsable items
        RecyclerView recyclerView = findViewById(R.id.recyclerview);
        final TaskListAdapter adapter = new TaskListAdapter(this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        // Sets up link between recycler view and database, UI will automatically update whenever database is updated
        mTaskViewModel = ViewModelProviders.of(this).get(TaskViewModel.class);
        mTaskViewModel.getAllTasks().observe(this, new Observer<List<Task>>() {
            @Override
            public void onChanged(List<Task> tasks) {
                // Updates the cache in the adapter
                adapter.setTasks(tasks);
            }
        });

        // Sets up reciever to recieve results from create new task and edit task
        LocalBroadcastManager.getInstance(this).registerReceiver(comandReciever,makeIntentFilter());
    }

    /**
     * Triggers on completion of activity to process data from new task activity and edit
     * task activity result. After task information is pulled from data intent a new task
     * object with the data is then passed to the view model which will update the database.
     * This way the the view model does not need to be passed to other activities.
     */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == NEW_TASK_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            String tempName = data.getStringExtra(NewTaskActivity.EXTRA_NEW_TASK_NAME);
            String tempDate = data.getStringExtra(NewTaskActivity.EXTRA_NEW_DUE_DATE);
            Boolean tempCompletion = data.getBooleanExtra(NewTaskActivity.EXTRA_NEW_COMPLETION,false);
            if (tempName != null){
                Task task = new Task(tempName, tempDate, tempCompletion);
                mTaskViewModel.insert(task);}
        } else if (requestCode == EDIT_TASK_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK){
            int tempId = data.getIntExtra(EditTaskActivity.EXTRA_EDIT_TASK_ID, -1);
            String tempName = data.getStringExtra(EditTaskActivity.EXTRA_EDIT_TASK_NAME);
            String tempDate = data.getStringExtra(EditTaskActivity.EXTRA_EDIT_DUE_DATE);
            Boolean tempCompletion = data.getBooleanExtra(EditTaskActivity.EXTRA_EDIT_COMPLETION,false);
            if (tempName != null || tempId == -1){
                Task task = new Task(tempId, tempName, tempDate, tempCompletion);
                mTaskViewModel.updateTask(task);}
        } else {
            Toast.makeText(
                    getApplicationContext(),
                    R.string.empty_not_saved,
                    Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Handles creation of the option menu in the action bar.
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    /**
     * Handle action bar item clicks here.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Sets up intent filter so task list adapter is not required to start activities or access
     * the task view model but can pass task info to this activity to initiate appropriate action.
     */
    private IntentFilter makeIntentFilter(){
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(TaskListAdapter.TaskViewHolder.CMD_TOGGLE);
        intentFilter.addAction(TaskListAdapter.TaskViewHolder.CMD_EDIT);
        intentFilter.addAction(TaskListAdapter.TaskViewHolder.CMD_DELETE);
        return intentFilter;
    }

    /**
     * Processes broadcasts from task list adapter to launch appropriate activity or initiate edits
     * via the task view model.
     */
    private final BroadcastReceiver comandReciever = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if(action.equals(TaskListAdapter.TaskViewHolder.CMD_TOGGLE)){
                Task tmpTask = new Task(intent.getIntExtra(TaskListAdapter.TaskViewHolder.EXTRA_TASK_ID,-1),intent.getStringExtra(TaskListAdapter.TaskViewHolder.EXTRA_TASK_NAME),intent.getStringExtra(TaskListAdapter.TaskViewHolder.EXTRA_DUE_DATE),intent.getBooleanExtra(TaskListAdapter.TaskViewHolder.EXTRA_IS_COMPLETED, false));
                Log.d("checked", "item id " + tmpTask.getId() + " toggle status: " + tmpTask.getIsCompleted());
                mTaskViewModel.toggleIsCompleted(tmpTask);                              // Marks selected task as completed
            } else if (action.equals(TaskListAdapter.TaskViewHolder.CMD_EDIT)){
                Intent editIntent = new Intent(MainActivity.this, EditTaskActivity.class);
                editIntent.putExtra(EditTaskActivity.EXTRA_EDIT_TASK_ID, intent.getIntExtra(TaskListAdapter.TaskViewHolder.EXTRA_TASK_ID,-1)); // TODO: check for -1 value for invalid id
                editIntent.putExtra(EditTaskActivity.EXTRA_EDIT_TASK_NAME, intent.getStringExtra(TaskListAdapter.TaskViewHolder.EXTRA_TASK_NAME));
                editIntent.putExtra(EditTaskActivity.EXTRA_EDIT_DUE_DATE, intent.getStringExtra(TaskListAdapter.TaskViewHolder.EXTRA_DUE_DATE));
                editIntent.putExtra(EditTaskActivity.EXTRA_EDIT_COMPLETION, intent.getBooleanExtra(TaskListAdapter.TaskViewHolder.EXTRA_IS_COMPLETED, false));
                startActivityForResult(editIntent, EDIT_TASK_ACTIVITY_REQUEST_CODE);    // Starts activity to edit selected task
            } else if (action.equals(TaskListAdapter.TaskViewHolder.CMD_DELETE)){
                Task tmpTask = new Task(intent.getIntExtra(TaskListAdapter.TaskViewHolder.EXTRA_TASK_ID,-1),intent.getStringExtra(TaskListAdapter.TaskViewHolder.EXTRA_TASK_NAME),intent.getStringExtra(TaskListAdapter.TaskViewHolder.EXTRA_DUE_DATE),intent.getBooleanExtra(TaskListAdapter.TaskViewHolder.EXTRA_IS_COMPLETED, false));
                mTaskViewModel.deleteTask(tmpTask);                                     // Deletes selected task
            }

        }
    };
}

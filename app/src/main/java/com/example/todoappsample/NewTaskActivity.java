package com.example.todoappsample;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;

import java.util.Calendar;

/**
 * This activity is launched from MainActivity.java to provide a user interface
 * to enter the data for a new task. This data is then passed backed to the
 * MainActivity.java as it is the only activity with access to the view model.
 * @author John Fielding
 */
public class NewTaskActivity extends AppCompatActivity {

    // Define string id's for components of the result intent
    public static final String EXTRA_NEW_TASK_NAME = "com.example.android.tasklistsql.NEW_TASK_NAME";
    public static final String EXTRA_NEW_DUE_DATE = "com.example.android.tasklistsql.NEW_DUE_DATE";
    public static final String EXTRA_NEW_COMPLETION = "com.example.android.tasklistsql.NEW_COMPLETION";

    // Stores handles for interactable UI elements
    private EditText mEditTaskView;
    private EditText mEditDateView;
    private CheckBox mEnableDateView;
    private CheckBox mIsCompletedView;
    private DatePickerDialog mDatePickerView;

    /**
     * On activity's creation, selected tasks details are imported into UI
     * and input listeners are set up.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_task);
        mEditTaskView = findViewById(R.id.edit_task);
        mEditDateView = findViewById(R.id.edit_date);
        mEnableDateView = findViewById(R.id.edit_date_picker_checkbox);
        mIsCompletedView = findViewById(R.id.edit_completed_checkbox);

        // If end date is enabled/disabled show/hide date view
        if(mEnableDateView.isChecked()) {
            mEditDateView.setVisibility(View.VISIBLE);
        } else {
            mEditDateView.setVisibility(View.GONE);
        }

        // Set on click listener for enabling/disabling end date to show/hide date view
        mEnableDateView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mEnableDateView.isChecked()){
                    mEditDateView.setVisibility(View.VISIBLE);
                } else {
                    mEditDateView.setVisibility(View.GONE);
                }
            }
        });

        mEditDateView.setInputType(InputType.TYPE_NULL);
        // On click listener to launch date picker if date view is visible
        mEditDateView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar calendar = Calendar.getInstance();
                int day = calendar.get(Calendar.DAY_OF_MONTH);
                int month = calendar.get(Calendar.MONTH);
                int year = calendar.get(Calendar.YEAR);
                // date picker dialog
                mDatePickerView = new DatePickerDialog(NewTaskActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                mEditDateView.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                            }
                        }, year, month, day);
                mDatePickerView.show();
            }
        });

        // on click listener for save button to set result intent and finish this activity
        final Button button = findViewById(R.id.edit_button_save);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent replyIntent = new Intent();
                if (TextUtils.isEmpty(mEditTaskView.getText())||(mEnableDateView.isChecked() && TextUtils.isEmpty(mEditDateView.getText()))) { //TODO: change to remaining in this activity and popping a toast for user feedback
                    setResult(RESULT_CANCELED, replyIntent);        //TODO: add cancel button with this as the result
                } else {
                    String task = mEditTaskView.getText().toString();
                    Boolean completionStatus = mIsCompletedView.isChecked();
                    replyIntent.putExtra(EXTRA_NEW_TASK_NAME, task);
                    replyIntent.putExtra(EXTRA_NEW_COMPLETION, completionStatus);
                    if(mEnableDateView.isChecked()){
                        String date = mEditDateView.getText().toString();
                        replyIntent.putExtra(EXTRA_NEW_DUE_DATE, date);
                    } else {
                        replyIntent.putExtra(EXTRA_NEW_DUE_DATE, "");
                    }
                    setResult(RESULT_OK, replyIntent);
                }
                finish();
            }
        });
    }
}
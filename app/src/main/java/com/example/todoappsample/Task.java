package com.example.todoappsample;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * Class that defines the tasks attributes and how it relates to the
 * database
 * @author John Fielding
 */
@Entity(tableName = "task_table")
public class Task {

    // Ensures that a unique id is always generated unless specified
    @PrimaryKey(autoGenerate = true)
    @NonNull
    private int id;

    // Remaining varibles are linked to their appropriate sql column
    @NonNull
    @ColumnInfo(name = "task")
    private String mTaskName;
    @ColumnInfo(name = "due_date")
    private String mDueDate; //TODO: change to long for easy calendar comparisons
    @NonNull
    @ColumnInfo(name = "is_completed")
    private Boolean mIsCompleted;

    /**
     * Primary constructor for task where id is automatically generated
     */
    public Task(@NonNull String taskName, String dueDate, @NonNull Boolean isCompleted){
        this.mTaskName = taskName;
        this.mDueDate = dueDate;
        this.mIsCompleted = isCompleted;
    }

    /**
     * Secondary constructor to manually set id. Used when updating data of an existing task.
     */
    public Task(@NonNull int taskId, @NonNull String taskName, String dueDate, @NonNull Boolean isCompleted){
        this.id = taskId;
        this.mTaskName = taskName;
        this.mDueDate = dueDate;
        this.mIsCompleted = isCompleted;
    }

    // Remaining functions are getters/setters
    public int getId() {return this.id;}

    public String getTaskName(){return this.mTaskName;}

    public String getDueDate(){return this.mDueDate;}

    public Boolean getIsCompleted(){return this.mIsCompleted;}

    public void setId(int id) { this.id = id;}

    public void setName(@NonNull String taskName){this.mTaskName = taskName;}

    public void setDueDate(String dueDate){this.mDueDate = dueDate;}

    public void toggleIsCompleted(){this.mIsCompleted = !this.mIsCompleted;}
}

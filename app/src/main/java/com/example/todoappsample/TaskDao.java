package com.example.todoappsample;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

/**
 * TaskDao is an object that provides an interface to the database
 * @author John Fielding
 */
@Dao
public interface TaskDao {

    @Insert
    void insert(Task task);

    @Query("DELETE FROM task_table")
    void deleteAll();

    @Query("DELETE FROM task_table WHERE id = :tid")
    void deleteTask(int tid);

    @Update
    void updateTask(Task... task);

    @Query("SELECT * from task_table ORDER BY task ASC") // TODO: add multiple queries to support sort function
    LiveData<List<Task>> getAllTasks();
}
package com.example.todoappsample;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

/**
 * Task list adapter contains the logic for how items in the recycler
 * view list can be interacted with and a broadcast manager to send
 * instructions to the main activity.
 * @author John Fielding
 */
public class TaskListAdapter extends RecyclerView.Adapter<TaskListAdapter.TaskViewHolder> {

    class TaskViewHolder extends RecyclerView.ViewHolder {
        // Strings for the broadcast intent id
        public static final String CMD_EDIT = "com.example.android.tasklistsql.EDIT";
        public static final String CMD_DELETE = "com.example.android.tasklistsql.DELETE";
        public static final String CMD_TOGGLE = "com.example.android.tasklistsql.TOGGLE";

        // Strings for id's of the extra data in the sent intent
        public static final String EXTRA_TASK_ID = "com.example.android.tasklistsql.ID";
        public static final String EXTRA_IS_COMPLETED = "com.example.android.tasklistsql.COMPLETED";
        public static final String EXTRA_TASK_NAME = "com.example.android.tasklistsql.NAME";
        public static final String EXTRA_DUE_DATE = "com.example.android.tasklistsql.DATE";

        // UI references for single task
        private final LinearLayout view;
        private final TextView taskItemView;
        private final TextView dueDateView;
        private final CheckBox isCompletedView;
        private final LinearLayout subItem;
        private final Button editBtnView;
        private final Button deleteBtnView;

        /**
         * Initializes UI references whenever a new task is added to the
         * recycler view list.
         */
        private TaskViewHolder(View itemView) {
            super(itemView);
            view = itemView.findViewById(R.id.recyclerview_item);
            taskItemView = itemView.findViewById(R.id.task_name);
            dueDateView = itemView.findViewById(R.id.due_date);
            isCompletedView = itemView.findViewById(R.id.is_completed_checkbox);
            subItem = itemView.findViewById(R.id.sub_item);
            editBtnView = itemView.findViewById(R.id.edit_btn);
            deleteBtnView = itemView.findViewById(R.id.delete_btn);
        }
    }

    private LocalBroadcastManager mLocalBroadcastManager;
    private Context mContext;
    private final LayoutInflater mInflater;
    private List<Task> mTasks; // cached copy of tasks

    TaskListAdapter(Context context) {mInflater = LayoutInflater.from(context);}

    /**
     * When view holder is created the broadcaster is initialised.
     */
    @Override
    public TaskViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        mLocalBroadcastManager = LocalBroadcastManager.getInstance(mContext);
        View itemView = mInflater.inflate(R.layout.recyclerview_item, parent, false);
        return new TaskViewHolder(itemView);
    }

    /**
     * When each task is added to the recycler view all ui and on click listeners are
     * initialised with that tasks data associated with it. This way there is no guess
     * work involved in tracking the position of the relevant task and what section of
     * the UI is being tapped. May not be memory efficient as each task will have it's
     * own set of on click listeners being stored in memory.
     */
    @Override
    public void onBindViewHolder(final TaskViewHolder holder, final int position){
        if(mTasks != null) {
            final Task current = mTasks.get(position);
            holder.taskItemView.setText(current.getTaskName());
            holder.dueDateView.setText(current.getDueDate());

            holder.isCompletedView.setChecked(current.getIsCompleted());
            holder.isCompletedView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //holder.isCompletedView.setChecked(!holder.isCompletedView.isChecked());
                    Log.d("checked", "item id " + current.getId() + " toggle status: " + holder.isCompletedView.isChecked());
                    Intent triggerToggle = new Intent(holder.CMD_TOGGLE);
                    triggerToggle.putExtra(holder.EXTRA_TASK_ID,current.getId());
                    triggerToggle.putExtra(holder.EXTRA_TASK_NAME,current.getTaskName());
                    triggerToggle.putExtra(holder.EXTRA_DUE_DATE,current.getDueDate());
                    triggerToggle.putExtra(holder.EXTRA_IS_COMPLETED, holder.isCompletedView.isChecked());
                    Log.d("checked", "item id " + current.getId() + " toggle status: " + holder.isCompletedView.isChecked());
                    mLocalBroadcastManager.sendBroadcast(triggerToggle);
                }
            });

            holder.taskItemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(View.VISIBLE == holder.subItem.getVisibility()){
                        holder.subItem.setVisibility(View.GONE);
                    } else {
                        holder.subItem.setVisibility(View.VISIBLE);
                    }
                    // notifyItemChanged(position);
                }
            });

            holder.editBtnView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Log.d("edit", "edit btn clicked. item id: " + current.getId());
                    Intent triggerEdit = new Intent(holder.CMD_EDIT);
                    triggerEdit.putExtra(holder.EXTRA_TASK_ID,current.getId());
                    triggerEdit.putExtra(holder.EXTRA_TASK_NAME,current.getTaskName());
                    triggerEdit.putExtra(holder.EXTRA_DUE_DATE,current.getDueDate());
                    triggerEdit.putExtra(holder.EXTRA_IS_COMPLETED, holder.isCompletedView.isChecked());
                    mLocalBroadcastManager.sendBroadcast(triggerEdit);
                }
            });

            holder.deleteBtnView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("delete", "delete btn clicked. item id: " + current.getId());
                    Intent triggerDelete = new Intent(holder.CMD_DELETE);
                    triggerDelete.putExtra(holder.EXTRA_TASK_ID,current.getId());
                    triggerDelete.putExtra(holder.EXTRA_TASK_NAME,current.getTaskName());
                    triggerDelete.putExtra(holder.EXTRA_DUE_DATE,current.getDueDate());
                    triggerDelete.putExtra(holder.EXTRA_IS_COMPLETED, holder.isCompletedView.isChecked());
                    mLocalBroadcastManager.sendBroadcast(triggerDelete);
                }
            });

            if(holder.isCompletedView.isChecked()){
                holder.view.setBackgroundColor(ContextCompat.getColor(mContext, R.color.taskComplete));
            } else {
                holder.view.setBackgroundColor(ContextCompat.getColor(mContext, R.color.taskIncomplete));
            }

        } else {
            // for when there's no data yet
            holder.taskItemView.setText("No Tasks Yet");
        }


    }

    void setTasks(List<Task> tasks){
        mTasks = tasks;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (mTasks != null)
            return mTasks.size();
        else return 0;
    }
}

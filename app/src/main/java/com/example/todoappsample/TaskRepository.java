package com.example.todoappsample;

import android.app.Application;
import android.os.AsyncTask;
import android.util.Log;

import androidx.lifecycle.LiveData;

import java.util.List;

/**
 * Task repostory is a class which implements the DAO interface with
 * an Async task manager so that the data base is updated on the UI
 * thread.
 * @author John Fielding
 */
public class TaskRepository {
    private TaskDao mTaskDao;
    private LiveData<List<Task>> mAllTasks;

    /**
     * Default constructor that stores a copy of the Room database and retrieves
     * the DAO of the room database
     */
    TaskRepository(Application application){
        TaskRoomDatabase db = TaskRoomDatabase.getDatabase(application);
        mTaskDao = db.taskDao();
        mAllTasks = mTaskDao.getAllTasks();
    }

    LiveData<List<Task>> getAllTasks() {
        return mAllTasks;
    }

    // Insert as an async task
    public void insert (Task task) {
        new insertAsyncTask(mTaskDao).execute(task);
    }

    private static class insertAsyncTask extends AsyncTask<Task, Void, Void> {

        private TaskDao mAsyncTaskDao;

        insertAsyncTask(TaskDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Task... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }

    // update as an Async task
    public void updateTask (Task task) {
        new updateTaskAsyncTask(mTaskDao).execute(task);
    }

    private static class updateTaskAsyncTask extends AsyncTask<Task, Void, Void> {

        private TaskDao mAsyncTaskDao;

        updateTaskAsyncTask(TaskDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Task... params) {
            mAsyncTaskDao.updateTask(params[0]);
            return null;
        }
    }

    // Toggling task completion as an async task
    public void toggleIsCompleted (Task task) {
        new toggleIsCompletedAsyncTask(mTaskDao).execute(task);
    }

    private static class toggleIsCompletedAsyncTask extends AsyncTask<Task, Void, Void> {

        private TaskDao mAsyncTaskDao;

        toggleIsCompletedAsyncTask(TaskDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Task... params) {
            Log.d("checked", "item id " + params[0].getId() + " future toggle status: " + params[0].getIsCompleted());
            mAsyncTaskDao.updateTask(params[0]);
            Log.d("checked", "item id " + params[0].getId() + " future toggle status: " + params[0].getIsCompleted());
            return null;
        }
    }

    // Deleting specified task as an async task
    public void deleteTask (Task task) {
        new deleteTaskAsyncTask(mTaskDao).execute(task.getId());
    }

    private static class deleteTaskAsyncTask extends AsyncTask<Integer, Void, Void> {

        private TaskDao mAsyncTaskDao;

        deleteTaskAsyncTask(TaskDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Integer... params) {
            mAsyncTaskDao.deleteTask(params[0]);
            return null;
        }
    }

}

package com.example.todoappsample;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

/**
 * By using Android Jetpack's (AndroidX's) Room Database changes in data
 * are easily observable and are preserved even if the app is killed. This
 * allows the app to easily maintain the state of the recycler view.
 * @author John Fielding
 */
@Database(entities = {Task.class}, version = 2, exportSchema = false)
public abstract class TaskRoomDatabase extends RoomDatabase {

    public abstract TaskDao taskDao();

    private static volatile TaskRoomDatabase INSTANCE;

    // makes database a singleton
    static TaskRoomDatabase getDatabase(final Context context){
        if (INSTANCE == null) {
            synchronized (TaskRoomDatabase.class) {
                if (INSTANCE == null){
                    // This is the actual database
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            TaskRoomDatabase.class, "task_database").fallbackToDestructiveMigration().build();
                }
            }
        }
        return INSTANCE;
    }
}

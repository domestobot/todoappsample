package com.example.todoappsample;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

/**
 * This view model allows indirect access to the database.
 * @author John Fielding
 */
public class TaskViewModel extends AndroidViewModel {

    private TaskRepository mRepository;

    private LiveData<List<Task>> mAllTasks;

    // Constructor initialises repository
    public TaskViewModel(Application application) {
        super(application);
        mRepository = new TaskRepository(application);
        mAllTasks = mRepository.getAllTasks();
    }

    LiveData<List<Task>> getAllTasks() {
        return mAllTasks;
    }

    public void insert(Task task) {
        mRepository.insert(task);
    }

    public void updateTask(Task task) {
        mRepository.updateTask(task);
    }

    public void toggleIsCompleted(Task task) {
        mRepository.toggleIsCompleted(task);
    }

    public void deleteTask(Task task) {
        mRepository.deleteTask(task);
    }
}
